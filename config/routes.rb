Rails.application.routes.draw do
  get 'api/index'

  post 'api/parse'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
