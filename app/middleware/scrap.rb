##
#Url::Scrap class
#
#@author::          LienPTB
#@usage::           index the content of a page include header tags (h1, h2 and h3) and links
#
##
require 'rubygems'
require 'nokogiri'   
require 'open-uri'

class Url::ContentScrap
  attr_reader :content, :error
  
  def initialize(url)
    @url = url
    @error = nil
    
    parse
  end
  
  protected
  
  def parse
    begin
      page = Nokogiri::HTML(open(@url))
      
      h1 = page.css('h1')
      h2 = page.css('h2')
      h3 = page.css('h3')
      links = page.css('a')
      
      @content = {
        'url' => @url,
        'h1' => h1.map{|h| h.text.squish.encode('UTF-8', :invalid => :replace, :undef => :replace, :replace => '')},
        'h2' => h2.map{|h| h.text.squish.encode('UTF-8', :invalid => :replace, :undef => :replace, :replace => '')},
        'h3' => h3.map{|h| h.text.squish.encode('UTF-8', :invalid => :replace, :undef => :replace, :replace => '')},
        'links' => links.map{|l| l['href']}.reject{|s| s.blank? || s.start_with?('#') || s.start_with?('/') || s.start_with?('javascript')}
      }
      
    rescue Exception => e
      @error = e
    end
  end
end