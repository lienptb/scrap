class ApiController < ApplicationController
  # List of previous parsed pages
  # Response:
  # +array+:: return array of pages that previously stored
  def index
    @list = Url.all
    render json: @list
  end

  # Index the content of a page
  # Params:
  # +url+:: url of the page
  # Response:
  # +object+:: return the content of the page after parsing (including url, h1, h2, h3 and links) if have
  # +success+:: return false if there is any problem
  # +error+:: error attached with the fail response
  def parse
    url = Url.where(url: params[:url]).first
    if url
      render json: url
    else
      parse = Url::ContentScrap.new(params[:url])
      if parse.error
        render json: {success: false, error: parse.error}
      else
        ob = Url.create(parse.content)
        ob.reload
        render json: ob
      end
    end
  end
end
