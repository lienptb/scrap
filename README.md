# README

This application is a tiny RESTFUL API that index the page content including header tags (h1, h2 and h3) and links inside the page and store it in database.

## Version

* Ruby 2.3.1
* Rails 5.0.0
* Postgres 9.3

## System dependencies

* Nokorigi gem

## API Specification

1. List the urls and content parsed previously  
*URL*: GET /api/index  
*Format*: JSON  
*Response*: array of page's content  

For example:  
```[{
id: 5,
url: "http://www.nokogiri.org/tutorials/installing_nokogiri.html",
h1: [
"Installing Nokogiri"
],
h2: [
"Ubuntu / Debian",
"FreeBSD"
],
h3: [
"Troubleshooting Ubuntu / Debian Installation",
"Troubleshooting RVM-based Installation"
],
links: [
"http://rdoc.info/github/sparklemotion/nokogiri",
"https://github.com/sparklemotion/nokogiri"
]
}]```

2. Parse an url, grab its content and store its content with the tags h1, h2 and h3  
*URL*: POST /api/parse  
*Format*: JSON  
*Params*:  
_url_: url of the page  
*Response*:  
__ the content of the page or success: false with error attached  


## Run Test  

``` rails test ```

## Deployment Architecture

![alt tag](imgpsh_fullsize.jpg)