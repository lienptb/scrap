require 'test_helper'

class ApiControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_index_url, as: :json
    assert_response :success
  end

  test "should post parse with params" do
    post api_parse_url, params: { url: 'http://google.com' }, as: :json
    assert_response :success
  end
  
  test "should post parse return error when there is invalid url" do
    post api_parse_url, params: { url: 'http://etbmanager.org/api/pub' }, as: :json
    
    json_response = ActiveSupport::JSON.decode @response.body
    assert_equal "404 Not Found", json_response['error']
  end
end
