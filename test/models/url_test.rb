require 'test_helper'

class UrlTest < ActiveSupport::TestCase
  
  test "should not save url without url" do
    url = Url.new
    assert_not url.save, "Saved the url without a url"
  end

end
